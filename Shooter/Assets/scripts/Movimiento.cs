﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public float movimientoSpeed = 1f;

    Vector3 targetPosition;
    Vector3 towarsTarget;

    float wandeRadius = 5f;

    void RecaculateTargetPosition()
    {
        targetPosition = transform.position + Random.insideUnitSphere * wandeRadius;
        targetPosition.y = 0;
    }

    private void Start()
    {
        RecaculateTargetPosition();
    }

    private void Update()
    {
        towarsTarget = targetPosition - transform.position;
        if (towarsTarget.magnitude < 0.25f)
            RecaculateTargetPosition();

        transform.position += towarsTarget.normalized * movimientoSpeed * Time.deltaTime;
        Debug.DrawLine(transform.position, targetPosition, Color.green);
    }
}

