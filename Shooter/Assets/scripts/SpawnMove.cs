﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMove : MonoBehaviour
{

    public GameObject blancos;
    public float timepodeSpawn = 0.05f, Rangocreacion = 2f;
    public static int objetivo;


    void Start()
    {
        objetivo = 0;
        InvokeRepeating("crearOb", 0.0f, timepodeSpawn);

    }

    void Update()
    {

        if (objetivo > 4)
        {
            CancelInvoke("crearOb");
        }
    }


    public void crearOb()
    {

        Vector3 SpawnPosition = new Vector3(0, 0, 0);
        SpawnPosition = this.transform.position + Random.onUnitSphere * Rangocreacion;
        SpawnPosition = new Vector3(SpawnPosition.x,
                                    SpawnPosition.y,
                                    SpawnPosition.z);

        GameObject cubo = Instantiate(blancos, SpawnPosition, Quaternion.identity);
        ++objetivo;

    }
}
