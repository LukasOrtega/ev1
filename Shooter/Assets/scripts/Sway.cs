﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sway : MonoBehaviour
{
    private Quaternion originLocalRotation;
    // Start is called before the first frame update
    void Start()
    {
        originLocalRotation = transform.localRotation;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        updateSway();
    }

    private void updateSway()
    {
        float t_xlookInput = Input.GetAxis("Mouse X");
        float t_ylookInput = Input.GetAxis("Mouse Y");

        //calculo rotacion

        Quaternion t_xAngleAdjustment = Quaternion.AngleAxis(-t_xlookInput * 1.45f, Vector3.up);
        Quaternion t_yAngleAdjustment = Quaternion.AngleAxis(t_xlookInput * 1.45f, Vector3.right);
        Quaternion t_targerRotation = originLocalRotation * t_xAngleAdjustment * t_yAngleAdjustment;

        transform.localRotation = Quaternion.Lerp(transform.localRotation, t_targerRotation, Time.deltaTime * 10f);
    }

}
