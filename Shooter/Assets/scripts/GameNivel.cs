﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameNivel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Facil()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void Normal()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void Difil()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void VolverMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}